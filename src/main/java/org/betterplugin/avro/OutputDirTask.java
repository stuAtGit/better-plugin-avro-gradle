/**
 * Copyright © 2013-2019 Commerce Technologies, LLC.
 *
 * Modified by Stu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.betterplugin.avro;

import java.io.File;
import javax.annotation.Nonnull;
import org.gradle.api.file.DirectoryProperty;
import org.gradle.api.file.FileCollection;
import org.gradle.api.file.FileTree;
import org.gradle.api.specs.Spec;
import org.gradle.api.tasks.OutputDirectory;
import org.gradle.api.tasks.PathSensitive;
import org.gradle.api.tasks.SourceTask;

import static org.gradle.api.tasks.PathSensitivity.RELATIVE;

class OutputDirTask extends SourceTask {
    private final DirectoryProperty outputDir;

    OutputDirTask() {
        this.outputDir = getProject().getObjects().directoryProperty();
    }

    public void setOutputDir(File outputDir) {
        this.outputDir.set(outputDir);
        getOutputs().dir(outputDir);
    }

    @Nonnull
    @PathSensitive(value = RELATIVE)
    public FileTree getSource() {
        return super.getSource();
    }

    @OutputDirectory
    protected DirectoryProperty getOutputDir() {
        return outputDir;
    }

    FileCollection filterSources(Spec<? super File> spec) {
        return getSource().filter(spec);
    }
}
