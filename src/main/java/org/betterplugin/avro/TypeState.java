/**
 * Copyright © 2015 Commerce Technologies, LLC.
 *
 * Modified by Stu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.betterplugin.avro;

import java.util.Set;
import java.util.TreeSet;
import org.apache.avro.Schema;
import org.gradle.api.GradleException;

class TypeState {
    private final String name;
    private final Set<String> locations = new TreeSet<>();
    private Schema schema;

    TypeState(String name) {
        this.name = name;
    }

    void processTypeDefinition(String path, Schema schemaToProcess) {
        locations.add(path);
        if (this.schema == null) {
            this.schema = schemaToProcess;
        } else if (!this.schema.equals(schemaToProcess)) {
            throw new GradleException(String.format("Found conflicting definition of type %s in %s", name, locations));
        } // Otherwise duplicate declaration of identical schema; nothing to do
    }

    String getName() {
        return name;
    }

    Schema getSchema() {
        return schema;
    }

    boolean hasLocation(String location) {
        return locations.contains(location);
    }
}
