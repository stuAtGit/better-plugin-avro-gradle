/*
 * Copyright © 2019 Commerce Technologies, LLC.
 *
 * Modified by Stu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.betterplugin.avro;

import org.gradle.api.Project;
import org.gradle.api.file.ConfigurableFileCollection;

class GradleCompatibility {
    static <T> T createExtensionWithObjectFactory(Project project, String extensionName, Class<T> extensionType) {
        if (GradleFeatures.extensionInjection.isSupported()) {
            return project.getExtensions().create(extensionName, extensionType);
        } else {
            return project.getExtensions().create(extensionName, extensionType, project.getObjects());
        }
    }

    @SuppressWarnings("deprecation")
    static ConfigurableFileCollection createConfigurableFileCollection(Project project) {
        if (GradleFeatures.objectFactoryFileCollection.isSupported()) {
            return project.getObjects().fileCollection();
        } else {
            return project.getLayout().configurableFiles();
        }
    }
}
