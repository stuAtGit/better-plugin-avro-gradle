/**
 * Copyright © 2015 Commerce Technologies, LLC.
 *
 * Modified by Stu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.betterplugin.avro;

import java.io.File;
import java.util.Set;
import java.util.TreeSet;

class FileState implements Comparable<FileState> {
    private final File file;
    private final String path;
    private String errorMessage;
    private Set<String> duplicateTypeNames = new TreeSet<>();

    FileState(File file, String path) {
        this.file = file;
        this.path = path;
    }

    File getFile() {
        return file;
    }

    Set<String> getDuplicateTypeNames() {
        return duplicateTypeNames;
    }

    void clearError() {
        errorMessage = null;
    }

    void setError(Throwable ex) {
        this.errorMessage = ex.getMessage();
    }

    void addDuplicateTypeName(String typeName) {
        duplicateTypeNames.add(typeName);
    }

    public boolean containsDuplicateTypeName(String typeName) {
        return duplicateTypeNames.contains(typeName);
    }

    public String getPath() {
        return path;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public int compareTo(FileState o) {
        return path.compareTo(o.getPath());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FileState fileState = (FileState) o;
        return path.equals(fileState.path);
    }

    @Override
    public int hashCode() {
        return path.hashCode();
    }
}
