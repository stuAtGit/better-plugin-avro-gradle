/**
 * Copyright © 2014-2019 Commerce Technologies, LLC.
 *
 * Modified by Stu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.betterplugin.avro;

import org.gradle.api.Plugin;
import org.gradle.api.Project;

import static org.betterplugin.avro.Constants.AVRO_EXTENSION_NAME;
import static org.betterplugin.avro.GradleCompatibility.createExtensionWithObjectFactory;

public class AvroBasePlugin implements Plugin<Project> {
    @Override
    public void apply(final Project project) {
        configureExtension(project);
    }

    private static void configureExtension(final Project project) {
        final AvroExtension avroExtension = createExtensionWithObjectFactory(project, AVRO_EXTENSION_NAME, DefaultAvroExtension.class);
        project.getTasks().withType(GenerateAvroJavaTask.class).configureEach(task -> {
            task.getOutputCharacterEncoding().convention(avroExtension.getOutputCharacterEncoding());
            task.getStringType().convention(avroExtension.getStringType());
            task.getFieldVisibility().convention(avroExtension.getFieldVisibility());
            task.getTemplateDirectory().convention(avroExtension.getTemplateDirectory());
            task.isCreateSetters().convention(avroExtension.isCreateSetters());
            task.isCreateOptionalGetters().convention(avroExtension.isCreateOptionalGetters());
            task.isGettersReturnOptional().convention(avroExtension.isGettersReturnOptional());
            task.isEnableDecimalLogicalType().convention(avroExtension.isEnableDecimalLogicalType());
            task.getDateTimeLogicalType().convention(avroExtension.getDateTimeLogicalType());
            task.getLogicalTypeFactories().convention(avroExtension.getLogicalTypeFactories());
            task.getCustomConversions().convention(avroExtension.getCustomConversions());
        });
    }
}
