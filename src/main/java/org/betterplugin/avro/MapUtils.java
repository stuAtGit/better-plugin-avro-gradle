/**
 * Copyright © 2015 Commerce Technologies, LLC.
 *
 * Modified by Stu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.betterplugin.avro;

import java.util.LinkedHashMap;
import java.util.Map;

class MapUtils {
    /**
     * Returns the map of all entries present in the first map but not present in the second map (by key).
     */
    static <K, V> Map<K, V> asymmetricDifference(Map<K, V> a, Map<K, V> b) {
        if (b == null || b.isEmpty()) {
            return a;
        }
        Map<K, V> result = new LinkedHashMap<>(a);
        result.keySet().removeAll(b.keySet());
        return result;
    }
}
