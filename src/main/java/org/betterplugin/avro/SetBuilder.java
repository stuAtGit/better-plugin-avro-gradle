/**
 * Copyright © 2013-2015 Commerce Technologies, LLC.
 *
 * Modified by Stu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.betterplugin.avro;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@SuppressWarnings("UnusedReturnValue")
class SetBuilder<T> {
    private Set<T> set = new HashSet<T>();

    SetBuilder<T> add(T e) {
        set.add(e);
        return this;
    }

    final SetBuilder<T> addAll(T[] c) {
        Collections.addAll(set, c);
        return this;
    }

    SetBuilder<T> addAll(Collection<? extends T> c) {
        set.addAll(c);
        return this;
    }

    SetBuilder<T> remove(T e) {
        set.remove(e);
        return this;
    }

    Set<T> build() {
        return set;
    }
}
