/**
 * Copyright © 2013-2019 Commerce Technologies, LLC.
 *
 * Modified by Stu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.betterplugin.avro;

import org.apache.avro.Conversion;
import org.apache.avro.LogicalTypes;
import org.gradle.api.provider.ListProperty;
import org.gradle.api.provider.MapProperty;
import org.gradle.api.provider.Property;

@SuppressWarnings("unused")
public interface AvroExtension {
    Property<String> getOutputCharacterEncoding();
    Property<String> getStringType();
    Property<String> getFieldVisibility();
    Property<String> getTemplateDirectory();
    Property<Boolean> isCreateSetters();
    Property<Boolean> isCreateOptionalGetters();
    Property<Boolean> isGettersReturnOptional();
    Property<Boolean> isEnableDecimalLogicalType();
    Property<String> getDateTimeLogicalType();
    MapProperty<String, Class<? extends LogicalTypes.LogicalTypeFactory>> getLogicalTypeFactories();
    ListProperty<Class<? extends Conversion<?>>> getCustomConversions();
    AvroExtension logicalTypeFactory(String typeName, Class<? extends LogicalTypes.LogicalTypeFactory> typeFactoryClass);
    AvroExtension customConversion(Class<? extends Conversion<?>> conversionClass);
}
