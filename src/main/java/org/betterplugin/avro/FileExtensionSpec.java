/**
 * Copyright © 2013-2015 Commerce Technologies, LLC.
 *
 * Modified by Stu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.betterplugin.avro;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.gradle.api.specs.Spec;

class FileExtensionSpec implements Spec<File> {
    private final Set<String> extensions;

    FileExtensionSpec(String... extensions) {
        this.extensions = new HashSet<>(Arrays.asList(extensions));
    }

    FileExtensionSpec(Collection<String> extensions) {
        this.extensions = new HashSet<>(extensions);
    }

    @Override
    public boolean isSatisfiedBy(File file) {
        return extensions.contains(FilenameUtils.getExtension(file.getName()));
    }
}
