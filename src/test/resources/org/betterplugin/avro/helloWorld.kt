/*
 * Copyright © 2017 Commerce Technologies, LLC.
 *
 * Modified by Stu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package demo

import example.avro.User
import java.time.LocalDate

fun main(args : Array<String>) {
    val user = User("Stu", 24, "blue", null, LocalDate.of(2019, 1, 1))
    println("Hello, ${user.getName()}")
}
