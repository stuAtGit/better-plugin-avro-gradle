/*
 * Copyright © 2015-2017 Commerce Technologies, LLC.
 *
 * Modified by Stu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.betterplugin.avro

import static org.gradle.testkit.runner.TaskOutcome.FAILED
import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class AvroPluginFunctionalSpec extends FunctionalSpec {
    def "setup"() {
        applyAvroPlugin()
        addDefaultRepository()
        addAvroDependency()
    }

    def "can generate and compile java files from json schema"() {
        given:
        copyResource("user.avsc", avroDir)

        when:
        def result = run()

        then:
        result.task(":generateAvroJava").outcome == SUCCESS
        result.task(":compileJava").outcome == SUCCESS
        projectFile(buildOutputClassPath("example/avro/User.class")).file
    }

    def "can generate and compile java files from json protocol"() {
        given:
        addAvroIpcDependency()
        copyResource("mail.avpr", avroDir)

        when:
        def result = run()

        then:
        result.task(":generateAvroJava").outcome == SUCCESS
        result.task(":compileJava").outcome == SUCCESS
        projectFile(buildOutputClassPath("org/apache/avro/test/Mail.class")).file
        projectFile(buildOutputClassPath("org/apache/avro/test/Message.class")).file
    }

    def "can generate and compile java files from IDL"() {
        given:
        copyResource("interop.avdl", avroDir)

        when:
        def result = run()

        then:
        result.task(":generateAvroProtocol").outcome == SUCCESS
        result.task(":generateAvroJava").outcome == SUCCESS
        result.task(":compileJava").outcome == SUCCESS
        projectFile(buildOutputClassPath("org/apache/avro/Foo.class")).file
        projectFile(buildOutputClassPath("org/apache/avro/Interop.class")).file
        projectFile(buildOutputClassPath("org/apache/avro/Kind.class")).file
        projectFile(buildOutputClassPath("org/apache/avro/MD5.class")).file
        projectFile(buildOutputClassPath("org/apache/avro/Node.class")).file
    }

    def "supports json schema files in subdirectories"() {
        given:
        copyResource("user.avsc", avroSubDir)

        when:
        def result = run()

        then:
        result.task(":generateAvroJava").outcome == SUCCESS
        result.task(":compileJava").outcome == SUCCESS
        projectFile(buildOutputClassPath("example/avro/User.class")).file
    }

    def "supports json protocol files in subdirectories"() {
        given:
        addAvroIpcDependency()
        copyResource("mail.avpr", avroSubDir)

        when:
        def result = run()

        then:
        result.task(":generateAvroJava").outcome == SUCCESS
        result.task(":compileJava").outcome == SUCCESS
        projectFile(buildOutputClassPath("org/apache/avro/test/Mail.class")).file
        projectFile(buildOutputClassPath("org/apache/avro/test/Message.class")).file
    }

    def "supports IDL files in subdirectories"() {
        given:
        copyResource("interop.avdl", avroSubDir)

        when:
        def result = run()

        then:
        result.task(":generateAvroProtocol").outcome == SUCCESS
        result.task(":generateAvroJava").outcome == SUCCESS
        result.task(":compileJava").outcome == SUCCESS
        projectFile(buildOutputClassPath("org/apache/avro/Foo.class")).file
        projectFile(buildOutputClassPath("org/apache/avro/Interop.class")).file
        projectFile(buildOutputClassPath("org/apache/avro/Kind.class")).file
        projectFile(buildOutputClassPath("org/apache/avro/MD5.class")).file
        projectFile(buildOutputClassPath("org/apache/avro/Node.class")).file
    }

    def "gives a meaningful error message when presented a malformed schema file"() {
        given:
        copyResource("enumMalformed.avsc", avroDir)
        def errorFilePath = new File("src/main/avro/enumMalformed.avsc").path

        when:
        def result = runAndFail()

        then:
        result.task(":generateAvroJava").outcome == FAILED
        result.output.contains("> Could not compile schema definition files:")
        result.output.contains("* $errorFilePath: \"enum\" is not a defined name. The type of the \"gender\" " +
                "field must be a defined name or a {\"type\": ...} expression.")
    }

    @SuppressWarnings(["GStringExpressionWithinString"])
    def "avro plugin correctly uses task configuration avoidance"() {
        given:
        buildFile << """
        |def configuredTasks = []
        |tasks.configureEach {
        |    configuredTasks << it
        |}        
        |gradle.buildFinished {
        |    println "Configured tasks: \${configuredTasks*.path}"
        |}
        |""".stripMargin()
        when:
        def result = run("help")

        then:
        def taskMatcher = result.output =~ /(?m)^Configured tasks: (.*)$/
        taskMatcher.find()
        def configuredTasks = taskMatcher.group(1)
        configuredTasks == "[:help]"
    }
}
