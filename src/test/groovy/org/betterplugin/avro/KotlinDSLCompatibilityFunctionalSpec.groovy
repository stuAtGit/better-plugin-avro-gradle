/*
 * Modified by Stu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.betterplugin.avro

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class KotlinDSLCompatibilityFunctionalSpec extends FunctionalSpec {
    File kotlinBuildFile

    def "setup"() {
        buildFile.delete() // Don't use the Groovy build file created by the superclass
        kotlinBuildFile = testProjectDir.newFile("build.gradle.kts")
        kotlinBuildFile << """
        |plugins {
        |    java
        |    id("org.betterplugin.avro")
        |}
        |repositories {
        |    jcenter()
        |}
        |dependencies {
        |    implementation("org.apache.avro:avro:${avroVersion}")
        |}
        |""".stripMargin()
    }

    def "works with kotlin DSL"() {
        given:
        copyResource("user.avsc", avroDir)

        when:
        def result = run()

        then:
        result.task(":generateAvroJava").outcome == SUCCESS
        result.task(":compileJava").outcome == SUCCESS
        projectFile(buildOutputClassPath("example/avro/User.class")).file
    }

    def "extension supports configuring all supported properties"() {
        given:
        copyResource("user.avsc", avroDir)
        kotlinBuildFile << """
        |avro {
        |    isCreateSetters.set(true)
        |    isCreateOptionalGetters.set(false)
        |    isGettersReturnOptional.set(false)
        |    fieldVisibility.set("PUBLIC_DEPRECATED")
        |    outputCharacterEncoding.set("UTF-8")
        |    stringType.set("String")
        |    templateDirectory.set(null as String?)
        |    isEnableDecimalLogicalType.set(true)
        |    dateTimeLogicalType.set("JSR310")
        |}
        |""".stripMargin()

        when:
        def result = run()

        then:
        result.task(":generateAvroJava").outcome == SUCCESS
        result.task(":compileJava").outcome == SUCCESS
        projectFile(buildOutputClassPath("example/avro/User.class")).file
    }
}
