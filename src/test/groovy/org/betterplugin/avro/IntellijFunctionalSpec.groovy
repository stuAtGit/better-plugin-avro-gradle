/*
 * Copyright © 2018 Commerce Technologies, LLC.
 *
 * Modified by Stu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.betterplugin.avro

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class IntellijFunctionalSpec extends FunctionalSpec {
    def "setup"() {
        applyAvroPlugin()
        applyPlugin("idea")
    }

    def "generated intellij project files include source directories for generated source"() {
        given:
        copyResource("user.avsc", avroDir)
        testProjectDir.newFolder("src", "main", "java")
        testProjectDir.newFolder("src", "test", "java")
        testProjectDir.newFolder("src", "test", "avro")

        when:
        run("idea")

        then:
        def moduleFile = new File(testProjectDir.root, "${testProjectDir.root.name}.iml")
        def module = new XmlSlurper().parseText(moduleFile.text)
        module.component.content.sourceFolder.findAll { it.@isTestSource.text() == "false" }.@url*.text().sort() == [
            'file://$MODULE_DIR\$/build/generated-main-avro-java',
            'file://$MODULE_DIR\$/src/main/avro', 'file://$MODULE_DIR\$/src/main/java',
        ]
        module.component.content.sourceFolder.findAll { it.@isTestSource.text() == "true" }.@url*.text().sort() == [
            'file://$MODULE_DIR\$/build/generated-test-avro-java',
            'file://$MODULE_DIR\$/src/test/avro', 'file://$MODULE_DIR\$/src/test/java',
        ]
    }

    def "generated output directories are created by default"() {
        when:
        def result = run("idea")

        then:
        result.task(":idea").outcome == SUCCESS
        projectFile("build/generated-main-avro-java").directory
        projectFile("build/generated-test-avro-java").directory
    }

    def "overriding task's outputDir doesn't result in default directory still being created"() {
        given:
        buildFile << """
        |tasks.named("generateAvroJava").configure {
        |    outputDir = file("build/generatedMainAvro")
        |}
        |tasks.named("generateTestAvroJava").configure {
        |    outputDir = file("build/generatedTestAvro")
        |}
        |""".stripMargin()

        when:
        def result = run("idea")

        then:
        result.task(":idea").outcome == SUCCESS
        !projectFile("build/generated-main-avro-java").directory
        !projectFile("build/generated-test-avro-java").directory
        projectFile("build/generatedMainAvro").directory
        projectFile("build/generatedTestAvro").directory
    }
}
