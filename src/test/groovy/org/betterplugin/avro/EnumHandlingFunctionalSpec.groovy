/*
 * Copyright © 2015-2016 Commerce Technologies, LLC.
 *
 * Modified by Stu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.betterplugin.avro

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

/**
 * Functional tests relating to handling of enums.
 */
class EnumHandlingFunctionalSpec extends FunctionalSpec {
    def "setup"() {
        applyAvroPlugin()
        addDefaultRepository()
        addAvroDependency()
    }

    def "supports simple enums"() {
        given:
        copyResource("enumSimple.avsc", avroDir)

        when:
        def result = run()

        then:
        result.task(":generateAvroJava").outcome == SUCCESS
        result.task(":compileJava").outcome == SUCCESS
        projectFile(buildOutputClassPath("example/avro/MyEnum.class")).file
    }

    def "supports enums defined within a record field"() {
        given:
        copyResource("enumField.avsc", avroDir)

        when:
        def result = run()

        then:
        result.task(":generateAvroJava").outcome == SUCCESS
        result.task(":compileJava").outcome == SUCCESS
        projectFile(buildOutputClassPath("example/avro/Test.class")).file
        projectFile(buildOutputClassPath("example/avro/Gender.class")).file
    }

    def "supports enums defined within a union"() {
        given:
        copyResource("enumUnion.avsc", avroDir)

        when:
        def result = run()

        then:
        result.task(":generateAvroJava").outcome == SUCCESS
        result.task(":compileJava").outcome == SUCCESS
        projectFile(buildOutputClassPath("example/avro/Test.class")).file
        projectFile(buildOutputClassPath("example/avro/Kind.class")).file
    }

    def "supports using enums defined in a separate schema file"() {
        given:
        copyResource("enumSimple.avsc", avroDir)
        copyResource("enumUseSimple.avsc", avroDir)

        when:
        def result = run()

        then:
        result.task(":generateAvroJava").outcome == SUCCESS
        result.task(":compileJava").outcome == SUCCESS
        projectFile(buildOutputClassPath("example/avro/User.class")).file
        projectFile(buildOutputClassPath("example/avro/MyEnum.class")).file
    }
}
