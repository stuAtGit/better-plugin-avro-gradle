/*
 * Copyright © 2018 Commerce Technologies, LLC.
 *
 * Modified by Stu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.betterplugin.avro

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class AvroBasePluginFunctionalSpec extends FunctionalSpec {
    def "setup"() {
        applyAvroBasePlugin()
    }

    def "can generate java files from json schema"() {
        given:
        buildFile << """
        |tasks.register("generateAvroJava", org.betterplugin.avro.GenerateAvroJavaTask) {
        |    source file("src/main/avro")
        |    include("**/*.avsc")
        |    outputDir = file("build/generated-main-avro-java")
        |}
        |""".stripMargin()

        copyResource("user.avsc", avroDir)

        when:
        def result = run("generateAvroJava")

        then:
        result.task(":generateAvroJava").outcome == SUCCESS
        projectFile("build/generated-main-avro-java/example/avro/User.java").file
    }

    def "can generate json schema files from json protocol"() {
        given:
        buildFile << """
        |tasks.register("generateSchema", org.betterplugin.avro.GenerateAvroSchemaTask) {
        |    source file("src/main/avro")
        |    include("**/*.avpr")
        |    outputDir = file("build/generated-main-avro-avsc")
        |}
        |""".stripMargin()

        copyResource("mail.avpr", avroDir)

        when:
        def result = run("generateSchema")

        then:
        result.task(":generateSchema").outcome == SUCCESS
        def expectedFileContents = getClass().getResource("Message.avsc").text.trim()
        def generateFileContents = new File(testProjectDir.root,
            "build/generated-main-avro-avsc/org/apache/avro/test/Message.avsc").text.trim()
        expectedFileContents == generateFileContents
    }

    def "can generate json schema files from IDL"() {
        given:
        buildFile << """
        |tasks.register("generateProtocol", org.betterplugin.avro.GenerateAvroProtocolTask) {
        |    source file("src/main/avro")
        |    outputDir = file("build/generated-avro-main-avpr")
        |}
        |tasks.register("generateSchema", org.betterplugin.avro.GenerateAvroSchemaTask) {
        |    dependsOn generateProtocol
        |    source file("build/generated-avro-main-avpr")
        |    include("**/*.avpr")
        |    outputDir = file("build/generated-main-avro-avsc")
        |}
        |""".stripMargin()

        copyResource("interop.avdl", avroDir)

        when:
        def result = run("generateSchema")

        then:
        result.task(":generateSchema").outcome == SUCCESS
        projectFile("build/generated-main-avro-avsc/org/apache/avro/Foo.avsc").file
        projectFile("build/generated-main-avro-avsc/org/apache/avro/Kind.avsc").file
        projectFile("build/generated-main-avro-avsc/org/apache/avro/MD5.avsc").file
        projectFile("build/generated-main-avro-avsc/org/apache/avro/Node.avsc").file
        projectFile("build/generated-main-avro-avsc/org/apache/avro/Interop.avsc").file
    }

    def "example of converting both IDL and json protocol simultaneously"() {
        given:
        buildFile << """
        |tasks.register("generateProtocol", org.betterplugin.avro.GenerateAvroProtocolTask) {
        |    source file("src/main/avro")
        |    include("**/*.avdl")
        |    outputDir = file("build/generated-avro-main-avpr")
        |}
        |tasks.register("generateSchema", org.betterplugin.avro.GenerateAvroSchemaTask) {
        |    dependsOn generateProtocol
        |    source file("src/main/avro")
        |    source file("build/generated-avro-main-avpr")
        |    include("**/*.avpr")
        |    outputDir = file("build/generated-main-avro-avsc")
        |}
        |""".stripMargin()

        copyResource("mail.avpr", avroDir)
        copyResource("interop.avdl", avroDir)

        when:
        def result = run("generateSchema")

        then:
        result.task(":generateSchema").outcome == SUCCESS
        projectFile("build/generated-main-avro-avsc/org/apache/avro/test/Message.avsc").file
        projectFile("build/generated-main-avro-avsc/org/apache/avro/Foo.avsc").file
        projectFile("build/generated-main-avro-avsc/org/apache/avro/Kind.avsc").file
        projectFile("build/generated-main-avro-avsc/org/apache/avro/MD5.avsc").file
        projectFile("build/generated-main-avro-avsc/org/apache/avro/Node.avsc").file
        projectFile("build/generated-main-avro-avsc/org/apache/avro/Interop.avsc").file
    }
}
