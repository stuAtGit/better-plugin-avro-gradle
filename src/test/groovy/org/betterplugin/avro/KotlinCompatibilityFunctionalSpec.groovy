/*
 * Copyright © 2017 Commerce Technologies, LLC.
 *
 * Modified by Stu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.betterplugin.avro

@SuppressWarnings(["Println"])
class KotlinCompatibilityFunctionalSpec extends FunctionalSpec {
    @SuppressWarnings(["FieldName"])
    protected static final String kotlinVersion = System.getProperty("kotlinVersion", "undefined")

    def "setup"() {
        println "Testing using Kotlin version ${kotlinVersion}."
    }

    def "works with kotlin-gradle-plugin"() {
        given:
        File kotlinDir = testProjectDir.newFolder("src", "main", "kotlin")
        applyAvroPlugin()
        applyPlugin("org.jetbrains.kotlin.jvm", kotlinVersion)
        applyPlugin("application")
        addDefaultRepository()
        addAvroDependency()
        addImplementationDependency("org.jetbrains.kotlin:kotlin-stdlib")
        addRuntimeDependency("joda-time:joda-time:2.9.9")
        buildFile << 'mainClassName = "demo.HelloWorldKt"'
        copyResource("user.avsc", avroDir)
        copyResource("helloWorld.kt", kotlinDir)

        when:
        def result = run("run")

        then:
        result.output.contains("Hello, Stu")
    }
}
