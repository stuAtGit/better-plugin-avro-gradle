/*
 * Copyright © 2019 David M. Carr
 *
 * Modified by Stu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.betterplugin.avro

import spock.lang.Subject

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

@Subject(GenerateAvroProtocolTask)
class GenerateAvroProtocolTaskFunctionalSpec extends FunctionalSpec {
    def "With base plugin, declares input on classpath"() {
        given: "a build that declares another task's output in the classpath"
        applyAvroBasePlugin()
        applyPlugin("java") // Jar task appears to only work with the java plugin applied
        buildFile << """
        |configurations.create("shared")
        |tasks.register("sharedIdlJar", Jar) {
        |    from "src/shared"
        |}
        |dependencies {
        |    shared sharedIdlJar.outputs.files  
        |}
        |tasks.register("generateProtocol", org.betterplugin.avro.GenerateAvroProtocolTask) {
        |    classpath = configurations.shared
        |    source file("src/dependent")
        |    outputDir = file("build/protocol")
        |}
        |""".stripMargin()
        
        copyResource("shared.avdl", testProjectDir.newFolder("src", "shared"))
        copyResource("dependent.avdl", testProjectDir.newFolder("src", "dependent"))

        when: "running the task"
        def result = run("generateProtocol")

        then: "running the generate protocol task occurs after running the producing task"
        result.tasks*.path == [":sharedIdlJar", ":generateProtocol"]
        result.task(":generateProtocol").outcome == SUCCESS
        projectFile("build/protocol/dependent.avpr").file
    }

    def "With avro plugin, declares input on classpath (runtime configuration by default)"() {
        given: "a build that declares another task's output in the classpath"
        applyAvroPlugin()
        buildFile << """
        |tasks.register("sharedIdlJar", Jar) {
        |    from "src/shared"
        |}
        |dependencies {
        |    runtimeOnly sharedIdlJar.outputs.files  
        |}
        |""".stripMargin()

        copyResource("shared.avdl", testProjectDir.newFolder("src", "shared"))
        copyResource("dependent.avdl", avroDir)

        when: "running the task"
        def result = run("generateAvroProtocol")

        then: "running the generate protocol task occurs after running the producing task"
        result.tasks*.path == [":sharedIdlJar", ":generateAvroProtocol"]
        result.task(":generateAvroProtocol").outcome == SUCCESS
        projectFile("build/generated-main-avro-avpr/dependent.avpr").file
    }
}
