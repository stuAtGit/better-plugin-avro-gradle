/*
 * Copyright © 2018 Commerce Technologies, LLC.
 *
 * Modified by Stu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.betterplugin.avro

import spock.lang.IgnoreIf
import spock.util.environment.OperatingSystem

import static org.gradle.testkit.runner.TaskOutcome.FROM_CACHE

/**
 * Testing for <a href="https://docs.gradle.org/current/userguide/build_cache.html">Build Cache</a> feature support.
 */
class BuildCacheSupportFunctionalSpec extends FunctionalSpec {
    def "setup"() {
        applyAvroPlugin()
        addDefaultRepository()
        addAvroDependency()
    }

    def "supports build cache for schema/protocol java source generation"() {
        given: "a project is built once with build cache enabled"
        copyResource("user.avsc", avroDir)
        copyResource("mail.avpr", avroDir)
        addAvroIpcDependency()
        run("build", "--build-cache")

        and: "the project is cleaned"
        run("clean")

        when: "the project is built again with build cache enabled"
        def result = run("build", "--build-cache")

        then: "the expected outputs were produced from the build cache"
        result.task(":generateAvroJava").outcome == FROM_CACHE
        result.task(":compileJava").outcome == FROM_CACHE
        projectFile("build/generated-main-avro-java/example/avro/User.java").file
        projectFile("build/generated-main-avro-java/org/apache/avro/test/Mail.java").file
        projectFile(buildOutputClassPath("example/avro/User.class")).file
        projectFile(buildOutputClassPath("org/apache/avro/test/Mail.class")).file
    }

    /**
     * This test appears to fail on Windows due to clean being unable to delete interop.avpr.
     */
    @IgnoreIf({ OperatingSystem.current.windows })
    def "supports build cache for IDL to protocol conversion"() {
        given: "a project is built once with build cache enabled"
        copyResource("interop.avdl", avroDir)
        run("build", "--build-cache")

        and: "the project is cleaned"
        run("clean")

        when: "the project is built again with build cache enabled"
        def result = run("build", "--build-cache")

        then: "the expected outputs were produced from the build cache"
        result.task(":generateAvroProtocol").outcome == FROM_CACHE
        result.task(":generateAvroJava").outcome == FROM_CACHE
        result.task(":compileJava").outcome == FROM_CACHE
        projectFile("build/generated-main-avro-avpr/interop.avpr").file
        projectFile("build/generated-main-avro-java/org/apache/avro/Interop.java").file
        projectFile(buildOutputClassPath("org/apache/avro/Interop.class")).file
    }

    def "supports build cache for protocol to schema conversion"() {
        given: "a project is built once with build cache enabled"
        copyResource("mail.avpr", avroDir)
        buildFile << """
        |tasks.register("generateSchema", org.betterplugin.avro.GenerateAvroSchemaTask) {
        |    source file("src/main/avro")
        |    include("**/*.avpr")
        |    outputDir = file("build/generated-main-avro-avsc")
        |}
        |""".stripMargin()
        run("generateSchema", "--build-cache")

        and: "the project is cleaned"
        run("clean")

        when: "the project is built again with build cache enabled"
        def result = run("generateSchema", "--build-cache")

        then: "the expected outputs were produced from the build cache"
        result.task(":generateSchema").outcome == FROM_CACHE
        projectFile("build/generated-main-avro-avsc/org/apache/avro/test/Message.avsc").file
    }
}
