/*
 * Copyright © 2013-2019 Commerce Technologies, LLC.
 *
 * Modified by Stu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.betterplugin.avro

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

class AvroPluginSpec extends Specification {
    Project project = ProjectBuilder.builder().build()

    def "avro protocol generation tasks are registered"() {
        when: "the plugin is applied to a project"
        project.apply(plugin: AvroPlugin)

        then: "avro protocol generation tasks are registered with the project with appropriate configuration"
        project.tasks.withType(GenerateAvroProtocolTask)*.name.sort() == ["generateAvroProtocol", "generateTestAvroProtocol"]
        mainGenerateAvroProtoTask.description == "Generates main Avro protocol definition files from IDL files."
        testGenerateAvroProtoTask.description == "Generates test Avro protocol definition files from IDL files."
        mainGenerateAvroProtoTask.group == Constants.GROUP_SOURCE_GENERATION
        testGenerateAvroProtoTask.group == Constants.GROUP_SOURCE_GENERATION
        // Can't easily test the sources
        mainGenerateAvroProtoTask.outputDir.get().asFile == project.file("build/generated-main-avro-avpr")
        testGenerateAvroProtoTask.outputDir.get().asFile == project.file("build/generated-test-avro-avpr")
    }

    def "avro java generation tasks are registered"() {
        when: "the plugin is applied to a project"
        project.apply(plugin: AvroPlugin)

        then: "avro java generation tasks are registered with the project with appropriate configuration"
        project.tasks.withType(GenerateAvroJavaTask)*.name.sort() == ["generateAvroJava", "generateTestAvroJava"]
        mainGenerateAvroJavaTask.description == "Generates main Avro Java source files from schema/protocol definition files."
        testGenerateAvroJavaTask.description == "Generates test Avro Java source files from schema/protocol definition files."
        mainGenerateAvroJavaTask.group == Constants.GROUP_SOURCE_GENERATION
        testGenerateAvroJavaTask.group == Constants.GROUP_SOURCE_GENERATION
        // Can't easily test the sources
        mainGenerateAvroJavaTask.outputDir.get().asFile == project.file("build/generated-main-avro-java")
        testGenerateAvroJavaTask.outputDir.get().asFile == project.file("build/generated-test-avro-java")
    }

    OutputDirTask getTask(String name) {
        return project.tasks.getByName(name) as OutputDirTask
    }

    OutputDirTask getMainGenerateAvroProtoTask() {
        return getTask("generateAvroProtocol")
    }

    OutputDirTask getTestGenerateAvroProtoTask() {
        return getTask("generateTestAvroProtocol")
    }

    OutputDirTask getMainGenerateAvroJavaTask() {
        return getTask("generateAvroJava")
    }

    OutputDirTask getTestGenerateAvroJavaTask() {
        return getTask("generateTestAvroJava")
    }
}
