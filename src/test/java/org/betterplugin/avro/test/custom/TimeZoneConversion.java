/*
 * Copyright © 2019 David M. Carr
 *
 * Modified by Stu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.betterplugin.avro.test.custom;

import java.util.TimeZone;
import org.apache.avro.Conversion;
import org.apache.avro.LogicalType;
import org.apache.avro.Schema;

@SuppressWarnings("unused")
public class TimeZoneConversion extends Conversion<TimeZone> {
    public static final String LOGICAL_TYPE_NAME = "timezone";

    @Override
    public Class<TimeZone> getConvertedType() {
        return TimeZone.class;
    }

    @Override
    public String getLogicalTypeName() {
        return LOGICAL_TYPE_NAME;
    }

    @Override
    public TimeZone fromCharSequence(CharSequence value, Schema schema, LogicalType type) {
        return TimeZone.getTimeZone(value.toString());
    }

    @Override
    public CharSequence toCharSequence(TimeZone value, Schema schema, LogicalType type) {
        return value.getID();
    }

    @Override
    public Schema getRecommendedSchema() {
        return TimeZoneLogicalType.INSTANCE.addToSchema(Schema.create(Schema.Type.STRING));
    }
}
