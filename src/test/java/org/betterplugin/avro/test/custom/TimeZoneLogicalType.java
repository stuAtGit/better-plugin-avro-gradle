/*
 * Copyright © 2019 David M. Carr
 *
 * Modified by Stu
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.betterplugin.avro.test.custom;

import org.apache.avro.LogicalType;
import org.apache.avro.Schema;

public class TimeZoneLogicalType extends LogicalType {
    static final TimeZoneLogicalType INSTANCE = new TimeZoneLogicalType();

    private TimeZoneLogicalType() {
        super(TimeZoneConversion.LOGICAL_TYPE_NAME);
    }

    @Override
    public void validate(Schema schema) {
        super.validate(schema);
        if (schema.getType() != Schema.Type.STRING) {
            throw new IllegalArgumentException("Timezone can only be used with an underlying string type");
        }
    }
}
